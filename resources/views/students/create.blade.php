@extends('layout')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Student</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('students' )}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Student</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{route('students.store') }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token() }}">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Name</label>
                    <div class="col-md-6"><input type="text" name="name" class="form-control"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Age</label>
                    <div class="col-md-6"><input type="text" name="age" class="form-control"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Gender</label>
                    <div class="col-md-6">
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="M">
                            <label style="font-weight: normal !important;">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="F">
                            <label style="font-weight: normal !important;">Female</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="other">
                            <label style="font-weight: normal !important;">Other</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Reporting Teacher</label>
                    <div class="col-md-6">
                    <select name="teacher" class="form-control">
                        <option value="">Choose Reporting Teacher</option>
                        <option value="Katie">Katie</option>
                        <option value="Max">Max</option>
                    </select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <input type="submit" class="btn btn-info" value="Save">
            </div>
        </form>
    </div>
</section>

@endsection