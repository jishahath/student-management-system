@extends('layout')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Student</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('students' )}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Edit Student</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{route('students.update', $student->id) }}" enctype="multipart/form-data">
        {{ method_field('PUT') }}{{csrf_field()}}
        <input type="hidden" name="_token" value="{{csrf_token() }}">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Name</label>
                    <div class="col-md-6"><input type="text" name="name" class="form-control" value="{{$student->name }}"></div>
                    <div class="clearfix"></div>
                </div>
            </div> 
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Age</label>
                    <div class="col-md-6"><input type="text" name="age" class="form-control" value="{{$student->age }}"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Gender</label>
                    <div class="col-md-6">
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="M" {{ $student->gender == 'M' ? 'checked' : '' }}>
                            <label style="font-weight: normal !important;">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="F" {{ $student->gender == 'F' ? 'checked' : '' }}>
                            <label style="font-weight: normal !important;">Female</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input type="radio" class="form-check-input" name="gender" value="other" {{ $student->gender == 'other' ? 'checked' : '' }}>
                            <label style="font-weight: normal !important;">Other</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Reporting Teacher</label>
                    <div class="col-md-6">
                    <select name="teacher" class="form-control">
                        <option value="">Choose Reporting Teacher</option>
                        <option value="Katie" {{ $student->teacher == 'Katie' ? 'selected' : '' }}>Katie</option>
                        <option value="Max" {{ $student->teacher == 'Max' ? 'selected' : '' }}>Max</option>
                    </select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-info" value="Save">
            </div>
        </form>
    </div>
</section>

@endsection