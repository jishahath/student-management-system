@extends('layout')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Students Record</h1>
            </div>
            <div class="col-sm-6">
                <ol class="nav nav-second-level">
                    <li class="breadcrumb-item {{ Request::segment(1) === 'students' ? 'active' : null }}"><a style= "text-decoration: none; color: inherit;" href="{{ url('students' )}}">Students</a></li>
                    <li class="breadcrumb-item {{ Request::segment(1) === 'marks' ? 'active' : null }}"><a style= "text-decoration: none; color: inherit;" href="{{ url('marks' )}}">Score Board</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <p>
            <a href="{{route('students.create') }}" class="btn btn-primary">Add New student</a>
        </p>
        <table class="table table-bordered table-stripped">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Reporting Teacher</th>
                <th>Action</th>
            </tr>
            @if(count($students))
            @foreach($students as $student) 
                <tr>
                    <td>{{$student->id}}</td>
                    <td>{{$student->name}}</td>
                    <td>{{$student->age}}</td>
                    <td>{{$student->gender}}</td>
                    <td>{{$student->teacher}}</td>
                    <td>
                        <a href="{{route('students.edit', $student->id) }}" class="btn btn-info">Edit</a> 
                        <a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger">Delete</a>
                        <form action="{{route('students.destroy', $student->id) }}" method="post">
                            @method('DELETE')
                            <input type="hidden" name="_token" value="{{csrf_token() }}">
                        </form>

                    </td>
                </tr>
            @endforeach
            @else
            <tr><td colspan="6">No Students Found</td></tr>
            @endif
        </table>
    </div>
</section>

@endsection