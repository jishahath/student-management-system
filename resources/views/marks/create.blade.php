@extends('layout')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Marks</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('marks' )}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Add Marks</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <form method="post" action="{{route('marks.store') }}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{csrf_token() }}">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Student Name</label>
                    <div class="col-md-6">
                    <select name="student_id" class="form-control">
                    <option value="">Choose Student</option>
                    @foreach($students as $student)
                    <option value="{{$student->id}}">{{$student->name}}</option>
                    @endforeach
                    </select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Maths</label>
                    <div class="col-md-6"><input type="text" name="maths" class="form-control"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Science</label>
                    <div class="col-md-6"><input type="text" name="science" class="form-control"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">History</label>
                    <div class="col-md-6"><input type="text" name="history" class="form-control"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <label class="col-md-3">Term</label>
                    <div class="col-md-6">
                    <select name="term" class="form-control">
                        <option value="">Choose Term</option>
                        <option value="One">One</option>
                        <option value="Two">Two</option>
                    </select>
                    <div class="clearfix"></div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <input type="submit" class="btn btn-info" value="Save">
            </div>
        </form>
    </div>
</section>

@endsection