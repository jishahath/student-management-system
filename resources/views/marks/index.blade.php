@extends('layout')
@section('content')


<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Score Board</h1>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-6">
                    <ol class="nav nav-second-level">
                        <li class="breadcrumb-item {{ Request::segment(1) === 'students' ? 'active' : null }}"><a style= "text-decoration: none; color: inherit;" href="{{ url('students' )}}">Students</a></li>
                        <li class="breadcrumb-item {{ Request::segment(1) === 'marks' ? 'active' : null }}"><a style= "text-decoration: none; color: inherit;" href="{{ url('marks' )}}">Score Board</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="content">
    <div class="container-fluid">
        <p>
            <a href="{{route('marks.create') }}" class="btn btn-primary">Add Marks</a>
        </p>
        <table class="table table-bordered table-stripped">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Maths</th>
                <th>Science</th>
                <th>History</th>
                <th>Term</th>
                <th>Total Marks</th>
                <th>Created On</th>
                <th>Action</th>
            </tr>
            @if(count($marks))
            @foreach($marks as $mark) 
                <tr>
                    <td>{{$mark->id}}</td>
                    <td>{{$mark->student->name}}</td>
                    <td>{{$mark->maths}}</td>
                    <td>{{$mark->science}}</td>
                    <td>{{$mark->history}}</td>
                    <td>{{$mark->term}}</td>
                    <?php $total_marks = (($mark->maths)+($mark->science)+($mark->history)); ?>
                    <td>{{$total_marks}}</td>
                    <td>{{ date_format($mark->created_at, 'M d, Y h:i A') }}</td>
                    <td>
                        <a href="{{route('marks.edit', $mark->id) }}" class="btn btn-info">Edit</a> 
                        <a href="javascript:void(0)" onclick="$(this).parent().find('form').submit()" class="btn btn-danger">Delete</a>
                        <form action="{{route('marks.destroy', $mark->id) }}" method="post">
                            @method('DELETE')
                            <input type="hidden" name="_token" value="{{csrf_token() }}">
                        </form>

                    </td>
                </tr>
            @endforeach
            @else
            <tr><td colspan="9">No marks Found</td></tr>
            @endif
        </table>
    </div>
</section>

@endsection
