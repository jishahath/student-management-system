**Step 1: download in your system.**

git clone https://gitlab.com/jishahath/student-management-system.git <br>
cd student-management-system
composer install

**Step 2: Configure your database from .env file**

DB_CONNECTION=mysql<br>
DB_HOST=127.0.0.1<br>
DB_PORT=3306<br>
DB_DATABASE=students_manager<br>
DB_USERNAME=root<br>
DB_PASSWORD=<br>

**Step 3: Install composer**

composer install

**Step 4: Run server**

php artisan serve

**Step 5: Create tables in database**

 php artisan migrate

 **Step 6: Run from your browser**

 http://localhost/student-management-system/
